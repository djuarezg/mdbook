# Mdbook image

This is a tool to convert Markdown into html. A way to build this in CERN's gitlab is:

```yaml
build:
  stage: build
  image: gitlab-registry.cern.ch/agonzale/mdbook:latest
  before_script:
    - mkdir public
  script:
    - mdbook build -d public
  artifacts:
    paths:
      - public

```

See the [upstream documentation](https://rust-lang-nursery.github.io/mdBook/) for more refence.